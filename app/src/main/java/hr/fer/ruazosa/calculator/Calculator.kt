package hr.fer.ruazosa.calculator

object Calculator {
    var result: Int = 0
    var expression: MutableList<String> = mutableListOf()

    fun reset() {
        result = 0
        expression = mutableListOf()
    }

    fun addNumber(number: String) {
        try {
            val num = number.toInt()
        } catch (e: NumberFormatException) {
            throw Exception("Not valid number")
        }
        if (expression.count() % 2 == 0) {
            expression.add(number)
        } else {
            throw Exception("Not a valid order of numbers in expression")
        }
    }

    fun addOperator(operator: String) {
        if (expression.count() % 2 != 1) {
            throw Exception("Not a valid order of operator in expression")
        }
        when (operator) {
            "+" -> expression.add(operator)
            "-" -> expression.add(operator)
            else -> {
                throw Exception("Not a valid operator")
            }
        }
    }

    fun evaluate() {
        if (expression.count() % 2 == 0) {
            throw Exception("Not a valid expression")
        }
        result = expression[0].toInt()
        for (i in 1..<expression.count() step 2) {
            when (expression[i]) {
                "+" -> result += expression[i + 1].toInt()
                "-" -> result -= expression[i + 1].toInt()
            }
        }
    }
}