package hr.fer.ruazosa.calculator

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CalculatorViewModel: ViewModel() {

    val calculationResult = MutableLiveData<String>()
    private var expression = mutableListOf<String>()
    private var numberToAddToExpression = "0"

    init {
        expression.add(numberToAddToExpression)
        calculationResult.value = numberToAddToExpression
    }

    fun resetCalculator() {
        Calculator.reset()
        numberToAddToExpression = "0"
        expression.clear()
        expression.add(numberToAddToExpression)
        calculationResult.value = numberToAddToExpression
    }

    fun evaluate() {
       for (i in expression.indices) {
           if (i % 2 == 0) {
               Calculator.addNumber(expression[i])
           }
           else {
               Calculator.addOperator(expression[i])
           }
       }
        Calculator.evaluate()
        expression.clear()
        expression.add(Calculator.result.toString())
        calculationResult.value = Calculator.result.toString()
        Calculator.reset()
    }

    fun addToExpression(numberOrOperator: String) {
        when (numberOrOperator) {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" -> {
                if (numberToAddToExpression == "0") {
                    numberToAddToExpression = numberOrOperator
                } else {
                    numberToAddToExpression += numberOrOperator
                }
                calculationResult.value = numberToAddToExpression
                expression[expression.size - 1] = numberToAddToExpression
            }
            "+" -> {
                if (expression.size % 2 == 1) {
                    expression.add("+")
                    numberToAddToExpression = "0"
                    expression.add(numberToAddToExpression)
                }


            }
            "-" -> {
                if (expression.size % 2 == 1) {
                    expression.add("-")
                    numberToAddToExpression = "0"
                    expression.add(numberToAddToExpression)
                }
            }
        }
    }
}