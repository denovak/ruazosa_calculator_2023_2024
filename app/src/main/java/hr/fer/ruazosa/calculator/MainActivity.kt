package hr.fer.ruazosa.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import hr.fer.ruazosa.calculator.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: CalculatorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layout = ActivityMainBinding.inflate(layoutInflater)
        setContentView(layout.root)

        viewModel = ViewModelProvider(this)[CalculatorViewModel::class.java]

        viewModel.calculationResult.observe(this) {
            layout.resultViewTextView.text = it
        }
        val addToViewModelExpression = {button: Button ->
            viewModel.addToExpression(button.text.toString())
        }
        val evaluateViewModel = {
            viewModel.evaluate()
        }

        layout.numberOneButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberTwoButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberThreeButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberFourButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberFiveButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberSixButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberSevenButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.numberEightButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }
        layout.numberNineButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }
        layout.numberZeroButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.plusButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.minusButton.setOnClickListener {
            addToViewModelExpression(it as Button)
        }

        layout.equalsButton.setOnClickListener { viewModel.evaluate() }

        layout.cancelButton.setOnClickListener { viewModel.resetCalculator() }
    }
}